{ pkgs ? import <nixpkgs> {}}:

pkgs.mkShell {
  nativeBuildInputs = ( with pkgs; [
    drawio
    elixir_1_14
    erlang
    fop
    inotify-tools
    ncurses
    openssl
    libressl
    libxml2
    libxslt
  ]);
}
