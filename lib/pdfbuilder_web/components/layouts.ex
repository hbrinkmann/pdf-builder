defmodule PdfbuilderWeb.Layouts do
  use PdfbuilderWeb, :html

  embed_templates "layouts/*"
end
