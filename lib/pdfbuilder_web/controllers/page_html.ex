defmodule PdfbuilderWeb.PageHTML do
  use PdfbuilderWeb, :html

  embed_templates "page_html/*"

  def formatted_price(price) do
    Number.Currency.number_to_currency(price)
  end

  def chunk_items(items, size, offset) do
    chunk_fun = fn element, acc ->
      if rem(element["number"] + offset, size) == 0 do
        {:cont, Enum.reverse([element | acc]), []}
      else
        {:cont, [element | acc]}
      end
    end

    after_fun = fn
      [] -> {:cont, []}
      acc -> {:cont, Enum.reverse(acc), []}
    end

    Stream.chunk_while(items, [], chunk_fun, after_fun)
    |> Enum.to_list
  end
end
