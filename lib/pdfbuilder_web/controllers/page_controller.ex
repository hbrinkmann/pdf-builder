defmodule PdfbuilderWeb.PageController do
  use PdfbuilderWeb, :controller

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.
    render(conn, :home, layout: false)
  end

  def process(conn, %{"json" => json_text, "format" => "html"}) do
    case Jason.decode(json_text) do
      {:ok, json} ->
        rendered = html_content(conn, json)
        html(conn, rendered)
      {:error, _} ->
        conn
        |> put_flash(:error, "Failed to handle JSON.")
        |> redirect(to: "/")
    end
  end

  def process(conn, %{"json" => json_text, "format" => "pdf"}) do
    case Jason.decode(json_text) do
      {:ok, json} ->
        rendered = html_content(conn, json)

        {:ok, pdf_base64_encoded} =
          [
            content: rendered,
            size: :a4
          ]
        |> ChromicPDF.Template.source_and_options()
        |> ChromicPDF.print_to_pdf(print_to_pdf: %{
          printBackground: true
        })

        {:ok, pdf} = Base.decode64(pdf_base64_encoded)
        send_download(conn, {:binary, pdf}, [filename: "download.pdf", content_type: "application/pdf", disposition: :inline])
      {:error, _} ->
        conn
        |> put_flash(:error, "Failed to handle JSON.")
        |> redirect(to: "/")
    end
  end

  defp html_content(conn, json) do
    IO.inspect(json)
    Phoenix.Template.render_to_string(view_module(conn), json["template"], "html", assign(conn, :json, json).assigns)
  end
end
